module.exports = function () {
    var instanceRoot = "C:\\inetpub\\wwwroot\\HelixStore";
    var config = {
        websiteRoot: instanceRoot + "\\Website",
        sitecoreLibraries: instanceRoot + "\\Website\\bin",
        licensePath: instanceRoot + "\\Data\\license.xml",
        solutionName: "HelixStore",
        buildConfiguration: "Debug",
        runCleanBuilds: false
    };
    return config;
}