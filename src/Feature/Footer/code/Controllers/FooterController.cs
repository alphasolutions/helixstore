﻿using System.Web.Mvc;
using Sitecore.Mvc.Controllers;

namespace AlphaSolutions.Feature.Footer.Controllers
{
    public class FooterController : SitecoreController
    {
        public ActionResult Rendering()
        {
            return View();
        }
    }
}