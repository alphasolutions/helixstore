﻿using System.Web.Mvc;
using Sitecore.Mvc.Controllers;

namespace AlphaSolutions.Feature.Header.Controllers
{
    public class HeaderController : SitecoreController
    {
        public ActionResult Rendering()
        {
            return View();
        }
    }
}